﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Windows.Speech;
using System;

public class VoiceMovement : MonoBehaviour
{
    [SerializeField] private KeywordRecognizer keywordRecognizer;
    [SerializeField] private Dictionary<string, Action> actions = new Dictionary<string, Action>();

    private void Start() {

        foreach (var device in Microphone.devices) {
            Debug.Log("Name: " + device);
        }


        actions.Add("frente", Forward);
        actions.Add("cima", Up);
        actions.Add("baixo", Down);
        actions.Add("afastar", Back);

        keywordRecognizer = new KeywordRecognizer(actions.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += RecognizedSpeech;
        keywordRecognizer.Start(); //começa o reconhecimento de fala
        //keywordRecognizer.Stop(); //pára de reconhecer a fala
    }

    public void RecognizedSpeech(PhraseRecognizedEventArgs speech) {
        Debug.Log(speech.text);
        actions[speech.text].Invoke();
    }

    private void Forward() {
        transform.Translate(0, 0, 1);
    }

    private void Up() {
        transform.Translate(0, 1, 0);
    }

    private void Down() {
        transform.Translate(0, -1, 0);
    }

    private void Back() {
        transform.Translate(0, 0, -1);
    }
}
