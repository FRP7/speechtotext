﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Windows.Speech;

/*
 * This script was taken from Unity Documentation:
 * https://docs.unity3d.com/ScriptReference/Windows.Speech.DictationRecognizer.html
 * */

public class WriteWords : MonoBehaviour
{
    /*
     * variable in which the speech sounds will be written
     */
    [SerializeField]
    private Text m_Hypotheses;
    //

    /*
     * variable in which the speech words will be written
     */
    [SerializeField]
    private Text m_Recognitions; 
    //

    private DictationRecognizer m_DictationRecognizer;

    void Start() {
        PhraseRecognitionSystem.Shutdown();
        //preparing dictation recognizer
        m_DictationRecognizer = new DictationRecognizer();
        //

        //write what the user is saying
        m_DictationRecognizer.DictationResult += (text, confidence) => {
            Debug.LogFormat("Dictation result: {0}", text);
            // m_Recognitions.text += text + " " +  "\n";
            m_Recognitions.text += text + " ";
        };
        //
        //i think this writes the sounds but not what the user really says
        m_DictationRecognizer.DictationHypothesis += (text) => {
            Debug.LogFormat("Dictation hypothesis: {0}", text);
            m_Hypotheses.text += text;
        };
        //
        //triggers when the recognizer session completes.
        m_DictationRecognizer.DictationComplete += (completionCause) => {
            if (completionCause != DictationCompletionCause.Complete)
                Debug.LogErrorFormat("Dictation completed unsuccessfully: {0}.", completionCause);
        };
        //

        //triggers when the recognizer session encouters an error.
        m_DictationRecognizer.DictationError += (error, hresult) => {
            Debug.LogErrorFormat("Dictation error: {0}; HResult = {1}.", error, hresult);
        };
        //

        m_DictationRecognizer.Start(); //starts speech recognition
    }
}
