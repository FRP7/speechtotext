﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckWords : MonoBehaviour
{
    [SerializeField] private Text userText; //text where user speech will be written
    [SerializeField] private string userString; //string where will contain userText
    [SerializeField] private string[] words; //list of words to be checked
    [SerializeField] private bool[] wordused; //list of bools to check on each word
    [SerializeField] private bool isCorrect = false; //verify if the user got all the words
    [SerializeField] private GameObject[] wordsUI; //gameobject of each UI word
    [SerializeField] private GameObject won; //won panel
    [SerializeField] private GameObject ExplainByYourself; //the current panel

    private void Update() {
        userString = userText.text; //put the userText value into a string
        CheckWord(); //verify if any of the words written is correct
        CheckIfCorrect(); //verify if all the isCorrect bools are true
        //verify if the user won
        if(isCorrect == true) {
            //Debug.Log("You won");
            ExplainByYourself.SetActive(false);
            won.SetActive(true);
        }
        //
    }

    //verify if any of the words written is correct
    private void CheckWord() {
        if(userString.Contains(words[0])) {
            wordused[0] = true;
            wordsUI[0].SetActive(false);
        }
        if (userString.Contains(words[1])) {
            wordused[1] = true;
            wordsUI[1].SetActive(false);
        }
        if (userString.Contains(words[2])) {
            wordused[2] = true;
            wordsUI[2].SetActive(false);
        }
    }

    //verify if all the isCorrect bools are true
    private void CheckIfCorrect() {
        foreach (bool checkbool in wordused) {
            if (!checkbool) {
                isCorrect = false;
            }
            if (checkbool) {
                isCorrect = true;
            }
        }
    }
}
