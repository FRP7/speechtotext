﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Windows.Speech;
using System;

/*
 * code based from this video:
 * https://www.youtube.com/watch?v=29vyEOgsW8s&t=1s
 */

public class VoiceAnswers : MonoBehaviour
{
    [SerializeField] private string[] answers;  //list of words the machine will detect
    [SerializeField] public KeywordRecognizer keywordRecognizer; 
    //dictionary where the words the player will say will be stored
    [SerializeField] private Dictionary<string, Action> actions = new Dictionary<string, Action>();

    [SerializeField] private GameObject CurrentQuestion; //current question page
    [SerializeField] private GameObject NextQuestion; //next question page

    private void Start() {

        //to check if the microphone is plugged in
        foreach (var device in Microphone.devices) {
            Debug.Log("Name: " + device);
        }

        /*
         * this will define which methods will call after
         * detecting certain words
         */
        actions.Add(answers[0], Correct); 
        actions.Add(answers[1], Wrong);
        actions.Add(answers[2], Wrong);
        //

        keywordRecognizer = new KeywordRecognizer(actions.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += RecognizedSpeech;
        keywordRecognizer.Start(); //start detection
        //keywordRecognizer.Stop(); //stop detection
    }

    public void RecognizedSpeech(PhraseRecognizedEventArgs speech) {
        Debug.Log(speech.text);
        actions[speech.text].Invoke();
    }

    //if correct, this method will change question
    private void Correct() {
        Debug.Log("Correcto");
        NextQuestion.SetActive(true);
        CurrentQuestion.SetActive(false);
    }

    //if wrong, this method will warn that is wrong
    private void Wrong() {
        Debug.Log("Errado");
    }
}
